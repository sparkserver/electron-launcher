#!/bin/sh

set -x

ORIG=$PWD
mkdir -p speedload_targets/win
mkdir -p speedload_targets/mac
mkdir -p speedload_targets/linux

cd SpeedLoad/SpeedLoadCli
xbuild /p:Configuration=Release
cd bin/Release
mkbundle --simple SpeedLoadCli.exe -o speedload-linux \
    --library libEasyLZMA.so \
    --machine-config /etc/mono/4.5/machine.config \
    -L /usr/lib/mono/4.5/Facades/ \
    --cross mono-5.12.0-ubuntu-14.04-x64 \
    --config /etc/mono/config
mkbundle --simple SpeedLoadCli.exe -o speedload-mac \
    --library libEasyLZMA.dylib \
    --machine-config /etc/mono/4.5/machine.config \
    -L /usr/lib/mono/4.5/Facades/ \
    --cross mono-5.12.0-osx-10.7-x64 \
    --config $ORIG/mono_config_macos
cp speedload-linux $ORIG/speedload_targets/linux/speedload
cp speedload-mac $ORIG/speedload_targets/mac/speedload
cp SpeedLoadCli.exe $ORIG/speedload_targets/win/speedload.exe
cp LibSpeedLoad.dll Easy.Common.dll AsyncEnumerable.dll Newtonsoft.Json.dll $ORIG/speedload_targets/win/
cd $ORIG