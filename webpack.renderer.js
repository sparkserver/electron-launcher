const webpack = require("webpack")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const VueLoaderPlugin = require("vue-loader/lib/plugin")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin")
const UglifyJsPlugin = require("terser-webpack-plugin")
const path = require("path")

const externals = {}
Object.keys(require("./package.json").dependencies).forEach(pkg => {
  if (pkg != "vue") {
    externals[pkg] = "commonjs " + pkg
  }
})

externals["ref"] = "commonjs ref"

const plugins = [
  new HtmlWebpackPlugin({
    template: "src/renderer/index.html",
    meta: {
      "Content-Security-Policy": {
        "http-equiv": "Content-Security-Policy",
        content:
          process.env.NODE_ENV === "production"
            ? "script-src 'self' 'unsafe-inline';"
            : "script-src 'self' 'unsafe-eval' 'unsafe-inline';"
      }
    }
  }),
  new VueLoaderPlugin(),
  new webpack.DefinePlugin({
    VERSION: '"' + require("./package.json").version + '"'
  })
]
let minimizer

if (process.env.NODE_ENV === "production") {
  plugins.push(new MiniCssExtractPlugin())
  minimizer = [
    new UglifyJsPlugin({
      sourceMap: true,
      parallel: true
    }),
    new OptimizeCssAssetsPlugin()
  ]
}

function getCSSMOneOf(extras) {
  if (typeof extras === "string") {
    extras = [extras]
  }
  if (!extras) {
    extras = []
  }
  const styleLoader =
    process.env.NODE_ENV !== "production"
      ? "vue-style-loader"
      : MiniCssExtractPlugin.loader
  return [
    {
      resourceQuery: /module/,
      use: [
        styleLoader,
        {
          loader: "css-loader",
          options: {
            modules: true,
            localIdentName:
              process.env.NODE_ENV === "production"
                ? "[hash:base64:8]"
                : "[local]_[hash:base64:8]"
          }
        },
        ...extras
      ]
    },
    {
      use: [styleLoader, "css-loader", ...extras]
    }
  ]
}

module.exports = {
  mode: process.env.NODE_ENV || "development",
  entry: "./src/renderer",
  output: {
    filename: "renderer.js",
    chunkFilename: "[name].renderer.js"
  },
  target: "electron-renderer",
  externals,
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: "vue-loader",
        options: {
          transformAssetUrls: {
            video: ["src", "poster"],
            source: "src",
            img: "src",
            image: "xlink:href",
            use: "xlink:href"
          }
        }
      },
      {
        test: /\.(svg|png|ttf|otf)$/,
        loader: "file-loader"
      },
      {
        test: /\.scss$/,
        oneOf: getCSSMOneOf("sass-loader")
      },
      {
        test: /\.css$/,
        oneOf: getCSSMOneOf()
      },
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    alias: {
      common: path.resolve(__dirname, "src/common"),
      lib: path.resolve(__dirname, "src/renderer/lib"),
      data: path.resolve(__dirname, "src/data")
    },
    extensions: [".js", ".json", ".jsx", ".css", ".vue", ".ts", ".tsx"]
  },
  devtool:
    process.env.NODE_ENV === "production"
      ? "hidden-source-map"
      : "cheap-eval-source-map",
  plugins,
  optimization: {
    minimizer
  }
}
