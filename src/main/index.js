require("common/sentryinit")
const { app, BrowserWindow, dialog, ipcMain } = require("electron")
const Sentry = require("@sentry/electron")
const url = require("url")
const path = require("path")
const os = require("os")
const uuid = require("uuid/v4")
const { autoUpdater } = require("electron-updater")
const { promptGameDirectory } = require("common/misc")
const Store = require("electron-store")
const store = new Store()
const logger = require("./logger")("Main")

function joinSplit(a, b) {
  const split = a.split(b)
  return [split.shift(), split.join(b)]
}

if (process.env.NODE_ENV !== "production" || store.get("debug", false)) {
  require("electron-debug")({
    enabled: true,
    showDevTools: false
  })
}

process.on("unhandledRejection", e => {
  throw e
})

let win
let sWin

ipcMain.on("open-servers", () => {
  if (sWin) {
    sWin.show()
    return sWin.focus()
  }
  sWin = new BrowserWindow({
    width: 640,
    height: 480,
    title: "Servers",
    backgroundColor: "#151e28",
    webPreferences: {
      devTools:
        process.env.NODE_ENV === "production"
          ? store.get("debug", false)
          : true,
      nodeIntegration: true
    }
  })
  sWin.setMenu(null)
  if (process.env.NODE_ENV === "production") {
    sWin.loadURL(
      url.format({
        pathname: path.join(__dirname, "index.html"),
        protocol: "file:",
        hash: `#/servers`,
        slashes: true
      })
    )
  } else {
    sWin.loadURL("http://localhost:8968/#/servers")
  }
  sWin.on("close", evt => {
    sWin.hide()
    evt.preventDefault()
  })
})

ipcMain.on("select-server", (_evt, value) => {
  win.webContents.send("select-server", value)
})

async function createWindow() {
  logger.log("Opening window")
  win = new BrowserWindow({
    width: 320,
    height: 520,
    resizable: false,
    frame: false,
    maximizable: false,
    // backgroundColor messes with font rendering on Windows
    backgroundColor: os.platform() !== "win32" ? "#161c24" : undefined,
    transparent: false,
    show: os.platform() !== "win32",
    webPreferences: {
      devTools:
        process.env.NODE_ENV === "production"
          ? store.get("debug", false)
          : true,
      nodeIntegration: true
    }
  })
  if (process.env.NODE_ENV === "production") {
    win.loadURL(
      url.format({
        pathname: path.join(__dirname, "index.html"),
        protocol: "file:",
        hash: `#/main/server/login`,
        slashes: true
      })
    )
  } else {
    win.loadURL("http://localhost:8968/#/main/server/login")
  }
  win.once("ready-to-show", () => {
    logger.log("Window ready-to-show")
    win.show()
  })
  win.on("close", () => {
    if (sWin) sWin.destroy()
    sWin = null
    win = null
  })
}

if (!app.requestSingleInstanceLock()) {
  dialog.showErrorBox("Error", "Another instance of launcher already running!")
  app.exit(1)
} else {
  app.on("ready", async () => {
    app.setAppUserModelId("eu.nextdata.launcher")
    if (process.defaultApp) {
      app.setAsDefaultProtocolClient("nfswlaunch", process.execPath, [
        `"${app.getAppPath()}"`
      ])
    } else {
      app.setAsDefaultProtocolClient("nfswlaunch")
    }
    autoUpdater.checkForUpdatesAndNotify()
    global.launchParams = {
      address: null,
      token: null,
      userId: null
    }
    for (const arg of process.argv.slice(1)) {
      if (arg.startsWith("nfswlaunch:")) {
        let uri = []
        {
          // nfswlaunch:[//.../.../?...]
          let path = joinSplit(arg, ":")[1]
          if (path.startsWith("//")) {
            // nfswlaunch://[.../.../?...]
            path = path.slice(2)
          }
          // nfswlaunch://[.../.../]?...
          path = path.split("?")[0]
          if (path.endsWith("/")) {
            // nfswlaunch://[.../...]/?...
            path = path.slice(0, path.length - 1)
          }
          // nfswlaunch://[...]/[...]/?...
          uri = path.split("/")
        }
        if (uri.length === 2 && uri[0] === "connect") {
          const addr = decodeURIComponent(uri[1])
          if (!addr.startsWith("http:") && !addr.startsWith("https:")) {
            global.launchParams.address = `http://${addr}/soapbox-race-core/Engine.svc`
          } else {
            global.launchParams.address = addr
          }
        }
        if (uri.length === 2 && uri[0] === "auth") {
          const ub = Buffer.from(uri[1], "base64").toString()
          const splits = ub.split("__")
          if (splits.length === 3) {
            global.launchParams = {
              address: splits[2],
              token: splits[1],
              userId: splits[0]
            }
          }
        }
      }
    }
    if (!store.has("userId")) {
      store.set("userId", uuid())
    }
    if (!store.has("installDir")) {
      const newGameDir = await promptGameDirectory(null)
      if (!newGameDir) return app.exit(0)
      store.set("installDir", newGameDir.dir)
      store.set("maxDlSize", newGameDir.maxDownload)
    }
    createWindow()
  })

  app.on("window-all-closed", async () => {
    console.log("Closing, dumping Sentry events")
    await Sentry.getCurrentHub()
      .getClient()
      .close()
    console.log("Done, quitting")
    app.quit()
  })
}

global.showLogFile = () => {
  const cp = require("child_process")
  const { logPath } = require("./logger")
  cp.spawn("explorer.exe", ["/select," + logPath])
}

global.restartApp = () => {
  app.relaunch()
  app.quit()
}

global.throwOutTheGarbage = () => {
  if (sWin) {
    sWin.destroy()
    sWin = null
  }
}
