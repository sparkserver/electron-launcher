const { app, ipcMain } = require("electron")
const path = require("path")
const fs = require("fs-extra")

const logPath = path.join(app.getPath("userData"), "log.txt")

const LogWriter = {
  queue: [],
  async init() {
    await fs.ensureDir(app.getPath("userData"))
    this.stream = fs.createWriteStream(logPath)
    this.queue.forEach(s => {
      this.stream.write(s)
    })
  },
  write(msg) {
    msg = `[${new Date().toISOString()}] ${msg}\r\n`
    if (this.stream) {
      this.stream.write(msg)
    } else {
      this.queue.push(msg)
    }
  }
}
ipcMain.on("write-log", (_evt, msg) => {
  LogWriter.write(msg)
})
LogWriter.init()

class NodeLogger {
  constructor(category) {
    this._category = category
  }

  log(msg) {
    const innerMsg = `[${this._category}] ${msg}`
    console.log(innerMsg)
    LogWriter.write(innerMsg)
  }
}

module.exports = category => {
  return new NodeLogger(category)
}

module.exports.logPath = logPath
