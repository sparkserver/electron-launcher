const got = require("got")
const { machineIdSync } = require("node-machine-id")

module.exports = got.extend({
  headers: {
    "User-Agent":
      "GameLauncher (+https://github.com/SoapboxRaceWorld/GameLauncher_NFSW)",
    "X-User-Agent": "electron-launcher (+https://gitlab.com/rbs-nfsw/electron-launcher)",
    "X-HWID": machineIdSync(),
    "X-GameLauncherHash": "4567"
  }
})
