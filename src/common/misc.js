const fs = require("fs")
const path = require("path")
const electron = require("electron")
const remote = electron.remote
const dialog = electron.dialog || electron.remote.dialog

function asyncErrorBox(title, message) {
  return dialog.showMessageBox(remote.getCurrentWindow(), {
    type: "error",
    title,
    message
  })
}

function asyncInfoBox(title, message) {
  return dialog.showMessageBox(remote.getCurrentWindow(), {
    type: "info",
    title,
    message
  })
}

function containsUnicodeChars(str) {
  for (let i = 0; i < str.length; i++) {
    if (str.charCodeAt(i) >= 128) {
      return true
    }
  }
  return false
}

async function promptGameDirectory(win) {
  while (true) {
    const { filePaths: files } = await dialog.showOpenDialog(win, {
      title: "Game directory?",
      properties: ["openDirectory"]
    })
    if (!files) {
      return null
    }
    if (containsUnicodeChars(files[0])) {
      await dialog.showMessageBox(win, {
        type: "error",
        buttons: ["OK"],
        message:
          "Path to game directory contains non-English characters. Please choose another directory."
      })
      continue
    }
    try {
      const file = path.join(files[0], ".wrchk")
      fs.writeFileSync(file, "write check")
      fs.unlinkSync(file)
      return {
        dir: files[0],
        maxDownload: fs.existsSync(path.join(files[0], "Tracks"))
      }
    } catch (e) {
      await dialog.showMessageBox(win, {
        type: "error",
        buttons: ["OK"],
        message: "Directory isn't writable!"
      })
    }
  }
}

export {
  promptGameDirectory,
  asyncErrorBox,
  asyncInfoBox,
  containsUnicodeChars
}
