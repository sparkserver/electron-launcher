import util = require("util")
import stream = require("stream")
import crypto = require("crypto")
import fs = require("fs")
import path = require("path")
import got = require("got")
import child_process = require("child_process")
import tmp = require("tmp")
const logger = require("./logger").default("Mods")
const pipeline = util.promisify(stream.pipeline)
const execFile = util.promisify(child_process.execFile)

interface ModInfo {
  basePath: string
  serverID: string
  features: string[]
}

interface ModEntry {
  Name: string
  Checksum: string
}

interface ModIndex {
  built_at: string
  entries: ModEntry[]
}

export async function getModInfo(serverUrl: string): Promise<ModInfo> {
  const resp = await got("/Modding/GetModInfo", {
    baseUrl: serverUrl,
    json: true
  })
  return resp.body
}

function serverIdHash(id: string): string {
  const hash = crypto.createHash("md5")
  hash.update(id)
  return hash.digest("hex")
}

function fileHash(path: string): Promise<string> {
  const hash = crypto.createHash("sha1")
  const inFile = fs.createReadStream(path)
  return new Promise((resolve, reject) => {
    inFile.on("data", chunk => hash.update(chunk))
    inFile.on("end", () => resolve(hash.digest("hex")))
    inFile.on("error", err => reject(err))
  })
}

async function downloadFile(
  outPath: string,
  url: string,
  baseUrl?: string,
  progress?: (percent: Number) => void
): Promise<void> {
  const outFile = fs.createWriteStream(outPath)
  const inStream = got.stream(url, { baseUrl })
  if (progress) {
    inStream.on("downloadProgress", p => progress(p.percent))
  }
  await pipeline(inStream, outFile)
}

function vcrtInstalled(version: string): boolean {
  const proc = child_process.spawnSync("reg", [
    "query",
    `HKLM\\SOFTWARE\\Microsoft\\VisualStudio\\${version}\\VC\\Runtimes\\x86`,
    "/reg:32"
  ])
  return proc.status == 0
}

export async function downloadRequiredFiles(
  gameDir: string,
  cb: (percent: Number) => void
) {
  const files = [
    "dinput8.dll",
    "global.ini",
    "7z.dll",
    "fmt.dll",
    "libcurl.dll",
    "zlib1.dll",
    "ModLoader.asi"
  ]
  const isVcrtInstalled = vcrtInstalled("14.0")
  let filesDownloaded = 0
  let totalFiles = files.length
  if (!isVcrtInstalled) {
    totalFiles++
  }
  for (const file of files) {
    const outPath = path.join(gameDir, file)
    await downloadFile(outPath, file, "http://cdn.soapboxrace.world/modules-v2")
    filesDownloaded++
    cb(filesDownloaded / totalFiles)
  }
  if (!isVcrtInstalled) {
    logger.log("Installing VC Redistributable")
    const outFile = tmp.tmpNameSync({ postfix: ".exe" })
    await downloadFile(
      outFile,
      "https://aka.ms/vs/16/release/vc_redist.x86.exe"
    )
    await execFile(outFile, ["/quiet", "/norestart"])
    fs.unlinkSync(outFile)
  }
}

export async function downloadMods(
  gameDir: string,
  info: ModInfo,
  cb: (percent: Number) => void
) {
  const modsDir = path.join(gameDir, "MODS", serverIdHash(info.serverID))
  fs.mkdirSync(modsDir, { recursive: true })
  logger.log("Downloading index")
  const indexRes = await got("index.json", {
    baseUrl: info.basePath,
    json: true
  })
  const mods = indexRes.body as ModIndex
  for (const mod of mods.entries) {
    const modPath = path.join(modsDir, mod.Name)
    if (fs.existsSync(modPath) && (await fileHash(modPath)) == mod.Checksum) {
      continue
    }

    logger.log("Downloading " + mod.Name)
    await downloadFile(modPath, mod.Name, info.basePath, cb)
  }
}

export async function cleanLinks(gameDir: string) {
  const linksFile = path.join(gameDir, ".links")
  if (!fs.existsSync(linksFile)) return

  const fileContents = fs.readFileSync(linksFile, "utf-8")
  for (const line of fileContents.split("\r\n")) {
    const lineSplits = line.split("\t")
    if (lineSplits.length < 2) continue
    const linkedFile = path.resolve(gameDir, lineSplits[0])

    if (!fs.existsSync(linkedFile)) {
      throw new Error(`Linked file ${linkedFile} does not exist`)
    }

    fs.unlinkSync(linkedFile)
    if (lineSplits[1] == "0") {
      const origPath = linkedFile + ".orig"
      if (fs.existsSync(origPath)) {
        fs.renameSync(origPath, linkedFile)
      }
    }
  }
  fs.unlinkSync(linksFile)
}
