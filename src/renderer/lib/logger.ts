const { ipcRenderer } = require("electron")

class BrowserLogger {
  readonly category: string

  constructor(category: string) {
    this.category = category
  }

  log(msg) {
    console.log(
      `%c${new Date().toISOString()} %c[${this.category}]`,
      "color: darkgrey;",
      "font-weight: bold",
      msg
    )
    ipcRenderer.send("write-log", `[${this.category}] ${msg}`)
  }
}

export default category => new BrowserLogger(category)
