import { GameProxy } from "./proxy"
import { DiscordPresence } from "./discord-rpc"
import { AuthToken } from "./auth"
const cp = require("child_process")

class LaunchManager {
  private _rpc: DiscordPresence

  constructor(discordRPC: DiscordPresence) {
    this._rpc = discordRPC
  }

  async launch(
    gamePath: string,
    server: string,
    token: AuthToken
  ): Promise<number> {
    const serverUrl = new URL(server)
    const proxy = new GameProxy(server.slice(0, -serverUrl.pathname.length))
    const proxyPort = await proxy.listenRandomPort()
    this._rpc.gameStart(proxy)
    return new Promise(resolve => {
      this._nativeLaunch(
        gamePath,
        [
          "SBRW",
          `http://127.0.0.1:${proxyPort}${serverUrl.pathname}`,
          token.token,
          token.userId
        ],
        code => {
          this._rpc.gameEnd()
          proxy.close()
          resolve(code)
        }
      )
    })
  }

  _nativeLaunch(
    gamePath: string,
    args: string[],
    cb: (Number) => void
  ): number {
    const proc = cp.spawn("nfsw.exe", args, {
      cwd: gamePath,
      stdio: ["ignore", process.stdout, process.stderr]
    })
    proc.on("exit", cb)
    return proc.pid
  }
}

export { LaunchManager }
