import Vue from "vue"

import Icon from "./Icon.vue"

function register() {
  Vue.component("icon", Icon)
}

export default register
