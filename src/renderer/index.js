process.env.NODE_ENV === "production" && require("common/sentryinit")
require("./lib/errors").registerHandlers()

import Vue from "vue"
import VueRouter from "vue-router"
import Vuex from "vuex"
import App from "./App.vue"
import Store from "electron-store"

import "./style/style.scss"
import "tachyons/src/_flexbox.css"

import router from "./router"

import registerComponents from "./components"

registerComponents()

const mixin = {
  computed: {
    appVersion: () => VERSION
  },
  beforeCreate() {
    const options = this.$options
    if (options.config) {
      this.$config = options.config
    } else if (options.parent && options.parent.$config) {
      this.$config = options.parent.$config
    }
  }
}

Vue.mixin(mixin)

Vue.use(VueRouter)
Vue.use(Vuex)

const configStore = new Store()

new Vue({
  el: "#app",
  render: h => h(App),
  config: configStore,
  router
})

export { configStore }
