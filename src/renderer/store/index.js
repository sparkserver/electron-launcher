import Vuex from "vuex"
import {
  default as got,
  HTTPError,
  TimeoutError,
  RequestError,
  ParseError
} from "got"
import { unhandledError } from "../lib/errors"
import { LaunchManager } from "../lib/launch"
import { copyResource } from "../lib/resource"
import { asyncErrorBox, asyncInfoBox } from "../../common/misc"
import { Downloader, DownloadTypes } from "../lib/downloader"
import * as auth from "../lib/auth"
import router from "../router"
import UA from "universal-analytics"
import * as mods from "../lib/mods"
import { remote } from "electron"
const fs = require("fs-extra")
const keytar = require("keytar")
const path = require("path")
const Sentry = require("@sentry/electron")
const logger = require("../lib/logger").default("Store")

function makeStore(configStore, discordRPC) {
  const ua = UA("UA-120377151-2", configStore.get("userId"))
  ua.set("aip", "1")
  return new Vuex.Store({
    state: {
      launchManager: new LaunchManager(discordRPC),
      analytics: ua,
      progress: {
        task: "",
        progress: 0,
        eta: null
      },
      downloadCompleted: false,
      currentServer: null,
      serverInformation: {},
      savedCredentials: null,
      serverLoaded: false,
      replicateMutations: false,
      externalLaunch: false
    },
    getters: {
      loggedInAs: state =>
        state.savedCredentials ? state.savedCredentials.email : null
    },
    mutations: {
      setCurrentServer(state, v) {
        state.currentServer = v
      },
      setServerInformation(state, v) {
        state.serverInformation = v
      },
      setSavedCredentials(state, v) {
        state.savedCredentials = v
      },
      setServerLoaded(state, v) {
        state.serverLoaded = v
      },
      setProgress(state, v) {
        state.progress = v
      },
      setDownloadCompleted(state) {
        state.downloadCompleted = true
      },
      setExternalLaunch(state) {
        state.externalLaunch = true
      }
    },
    actions: {
      async loadServerInformation({ state, commit, dispatch }) {
        try {
          const res = await got(`${state.currentServer}/GetServerInformation`, {
            json: true
          })
          commit("setServerInformation", res.body)
        } catch (e) {
          dispatch("handleError", e)
        }
      },
      async loadSavedCredentials({ state, commit }) {
        const url = new URL(state.currentServer)
        const creds = await keytar.findCredentials(`nfsw(${url.host})`)
        console.log(creds)
        if (creds.length === 0) commit("setSavedCredentials", null)
        else
          commit("setSavedCredentials", {
            email: creds[0].account,
            password: creds[0].password
          })
      },
      async saveCredentials({ state, commit }, creds) {
        const url = new URL(state.currentServer)
        await keytar.setPassword(
          `nfsw(${url.host})`,
          creds.email,
          creds.password
        )
        commit("setSavedCredentials", creds)
      },
      async deleteSavedCredentials({ state, commit }) {
        const url = new URL(state.currentServer)
        const creds = await keytar.findCredentials(`nfsw(${url.host})`)
        for (const cred of creds) {
          await keytar.deletePassword(`nfsw(${url.host})`, cred.account)
        }
        commit("setSavedCredentials", null)
      },
      async setCurrentServer({ state, dispatch, commit }, server) {
        commit("setServerLoaded", false)
        commit("setCurrentServer", server)
        Sentry.configureScope(scope => {
          scope.setTag("server", new URL(server).host)
        })
        await dispatch("loadServerInformation")
        discordRPC.setServerName(
          state.serverInformation.serverName || "¯\\_(ツ)_/¯"
        )
        await dispatch("loadSavedCredentials")
        configStore.set("lastServer", server)
        commit("setServerLoaded", true)
      },
      async login({ state, dispatch }, creds) {
        try {
          let token
          if (
            state.serverInformation.modernAuthSupport &&
            state.currentServer.startsWith("https")
          ) {
            token = await auth.loginModern(state.currentServer, creds)
          } else {
            token = await auth.loginLegacy(state.currentServer, creds)
          }
          if (token.warning) {
            await asyncInfoBox("Warning from server", token.warning)
          }
          if (configStore.get("rememberMe", false)) {
            await dispatch("saveCredentials", creds)
          }
          return dispatch("launchGame", token)
        } catch (e) {
          dispatch("handleError", e)
        }
      },
      async register({ state, dispatch }, creds) {
        try {
          if (
            state.serverInformation.modernAuthSupport &&
            state.currentServer.startsWith("https")
          ) {
            await asyncInfoBox(
              "Message",
              await auth.registerModern(state.currentServer, creds)
            )
          } else {
            const token = await auth.registerLegacy(state.currentServer, creds)
            if (token.warning) {
              await asyncInfoBox("Warning from server", token.warning)
            }
            if (configStore.get("rememberMe", false)) {
              await dispatch("saveCredentials", creds)
            }
            return dispatch("launchGame", token)
          }
        } catch (e) {
          dispatch("handleError", e)
        }
      },
      async launchGame({ state, commit }, token) {
        state.analytics.event("Game", "Launch", state.currentServer).send()
        setInterval(() => {
          state.analytics.event("Game", "Heartbeat").send()
        }, 300 * 1000)
        const gd = configStore.get("installDir")
        try {
          fs.unlinkSync(path.join(gd, "ModManager.asi"))
          fs.unlinkSync(path.join(gd, "lightfx.dll"))
        } catch (e) {}
        await mods.cleanLinks(gd)
        let modInfo = null
        try {
          modInfo = await mods.getModInfo(state.currentServer)
        } catch (e) {}
        if (modInfo) {
          let progress = v => {
            commit("setProgress", {
              task: "Downloading required files",
              progress: v,
              eta: null
            })
          }
          await mods.downloadRequiredFiles(gd, progress)
          progress = v => {
            commit("setProgress", {
              task: "Downloading server mods",
              progress: v,
              eta: null
            })
          }
          progress(0)
          await mods.downloadMods(gd, modInfo, progress)
        }
        remote.getGlobal("throwOutTheGarbage")()
        const p = state.launchManager.launch(gd, state.currentServer, token)
        router.push("/main/starting")
        let res = 0
        try {
          res = await p
        } catch (e) {
          if (e.code === "ENOENT") {
            await asyncErrorBox(
              "Error",
              "Game executable not found. Please select correct game directory in settings."
            )
          } else {
            throw e
          }
        }
        await mods.cleanLinks(gd)
        if (res != 0) {
          const msg = `Game exited with errorcode ${res} (0x${res
            .toString(16)
            .padStart(8, "0")})`
          if (Math.random() < 0.1) {
            Sentry.captureMessage(msg)
          }
          const sClient = Sentry.getCurrentHub().getClient()
          if (sClient) {
            await sClient.close()
          }
          await asyncErrorBox("Error", msg)
        }
        window.close()
      },
      async startDownloader({ commit }) {
        const dl = new Downloader({
          downloadTypes: [
            DownloadTypes.base,
            configStore.get("maxDlSize", false)
              ? DownloadTypes.tracks
              : DownloadTypes.tracksHigh,
            DownloadTypes.speech
          ],
          cdnUrl: configStore.get("cdnUrl"),
          directory: configStore.get("installDir")
        })
        dl.on("progress", p => {
          commit("setProgress", p)
        })
        const cdnList = (
          await got(
            "https://gist.githubusercontent.com/redbluescreen/326c0b184dddaca19594cccc47baf4f6/raw/cdnlist.json",
            { json: true }
          )
        ).body.map(v => v.url)
        if (
          !configStore.get("cdnUrl") ||
          cdnList.indexOf(configStore.get("cdnUrl")) === -1
        ) {
          configStore.set("cdnUrl", await dl.chooseCDN(cdnList))
        }
        await dl.loadIndex()
        await dl.download()
        commit("setProgress", {
          task: "Download completed",
          progress: 1,
          eta: null
        })
        commit("setDownloadCompleted")
      },
      handleError({}, error) {
        if (error instanceof RequestError) {
          // Network error
          asyncErrorBox(
            "Error",
            "Failed to fetch data from server. Maybe you have no internet or the server is down?"
          )
        } else if (error instanceof TimeoutError) {
          // Timeout
          asyncErrorBox(
            "Error",
            "Request to server timed out. Maybe the server is down?"
          )
        } else if (error instanceof HTTPError) {
          logger.log(`HTTP Error ${error.statusCode}`)
          logger.log(error.body)
          // Status != 2xx
          asyncErrorBox(
            "Error",
            `Server responded with an error (${error.statusCode} ${error.statusMessage})`
          )
        } else if (
          error instanceof ParseError ||
          error instanceof auth.XMLParseError
        ) {
          asyncErrorBox("Error", "Server returned invalid response")
        } else if (error instanceof auth.AuthError) {
          // Error on login/register
          asyncErrorBox("Authentication error", error.message)
        } else {
          unhandledError(error)
        }
      }
    }
  })
}

export default makeStore
