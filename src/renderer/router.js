import VueRouter from "vue-router"

const router = new VueRouter({
  routes: [
    {
      path: "/main",
      component: () => import(/* webpackChunkName: "mainw" */ "./MainWindow"),
      children: [
        {
          path: "server",
          component: () =>
            import(/* webpackChunkName: "mainw" */ "./ServerPage"),
          children: [
            {
              path: "login",
              component: () =>
                import(/* webpackChunkName: "mainw" */ "./LoginPage")
            },
            {
              path: "register",
              component: () =>
                import(/* webpackChunkName: "mainw" */ "./RegisterPage")
            }
          ]
        },
        {
          path: "starting",
          component: () =>
            import(/* webpackChunkName: "mainw" */ "./GameStartingPage")
        },
        {
          path: "settings",
          component: () =>
            import(/* webpackChunkName: "mainw" */ "./SettingsPage")
        }
      ]
    },
    {
      path: "/servers",
      component: () =>
        import(/* webpackChunkName: "servers" */ "./ServersWindow")
    }
  ]
})

export default router
