const webpack = require("webpack")
const UglifyJsPlugin = require("terser-webpack-plugin")
const path = require("path")

const externals = {}
Object.keys(require("./package.json").dependencies).forEach(pkg => {
  externals[pkg] = "commonjs " + pkg
})
Object.keys(require("./package.json").devDependencies).forEach(pkg => {
  externals[pkg] = "commonjs " + pkg
})

const plugins = [
  new webpack.DefinePlugin({
    VERSION: '"' + require("./package.json").version + '"'
  })
]

module.exports = {
  mode: process.env.NODE_ENV || "development",
  entry: "./src/main",
  output: {
    filename: "main.js"
  },
  externals,
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: ["ts-loader"],
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
    alias: {
      common: path.resolve(__dirname, "src/common")
    }
  },
  target: "electron-main",
  node: {
    __dirname: false,
    __filename: false
  },
  devtool:
    process.env.NODE_ENV !== "production"
      ? "cheap-module-eval-source-map"
      : undefined,
  optimization: {
    minimizer:
      process.env.NODE_ENV === "production"
        ? [
            new UglifyJsPlugin({
              sourceMap: true,
              parallel: true,
              extractComments: true
            })
          ]
        : undefined
  },
  plugins
}
